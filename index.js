const express = require("express")
const mongoose = require("mongoose")

const app = express()

const port = 4000

mongoose.connect("mongodb+srv://User1:lokomoko12@cluster0.hy0hl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

let dataBase = mongoose.connection

dataBase.on("error",console.error.bind(console,"connection error"))

dataBase.once("open",()=>console.log("Connected to MongoDB"))

app.use(express.json())

const taskRoutes = require('./routes/taskRoutes')
app.use('/tasks',taskRoutes)


const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);



app.listen(port,()=>console.log(`Server running at port ${port}`))