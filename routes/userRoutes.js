const express = require('express')
const router = express.Router()

const userControllers =  require('../controllers/userControllers')
const {

	createUserController,
	getAllUsersController,
	getSingleUserController,
	updateSingleUserController

} = userControllers

router.post('/', createUserController)

router.get('/', getAllUsersController)

router.get('/:id', getSingleUserController)

router.put('/:id', updateSingleUserController)

module.exports = router;