const express = require('express')
const router = express.Router()
const taskControllers = require('../controllers/taskControllers');

const {
	createTaskController,
	getAllTasksController,
	getSingleTaskController,
	updateTaskController,
	getAllPending,
	getAllComplete,
	getAllCancelled
} = taskControllers

router.post('/', createTaskController)

router.get('/', getAllTasksController)

router.get('/getAllPending', getAllPending)

router.get('/getAllComplete', getAllComplete)

router.get('/getAllCancelled', getAllCancelled)

router.get('/:id',getSingleTaskController)

router.put('/:id', updateTaskController)

module.exports = router