const Task = require('../models/task')

module.exports.createTaskController = (req,res)=>{
	console.log(req.body)
	Task.findOne({name : req.body.name})
	.then(result => {
		console.log(result)
		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate Task Found")
		} 
		else {
			let newTask = new Task({
			name: req.body.name,
			status: req.body.status
			})
			newTask.save()
			.then(result => res.send(result))
			.catch(err => res.send(err))
		}
	})
	.catch(err => res.send(err));
}

module.exports.getAllTasksController = (req,res)=>{
	Task.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.getSingleTaskController = (req,res) => {
	console.log(req.params);
	Task.findById(req.params.id,{_id:0,name:1})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateTaskController = (req,res) =>{
	console.log(req.params.id)
	let updateTask = {
		name:req.body.name,
		status:req.body.status
	}
	Task.findByIdAndUpdate(req.params.id,updateTask,{new:true})
	.then(updatedTask => res.send(updatedTask))
	.catch(err => res.send(err))
}

module.exports.getAllPending = (req,res) => {
	Task.find({status:"pending"})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.getAllComplete = (req,res) => {
	Task.find({status:"complete"})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.getAllCancelled = (req,res) => {
	Task.find({status:"cancelled"})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}