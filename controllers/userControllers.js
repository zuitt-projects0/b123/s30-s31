const User = require("../models/user")

module.exports.createUserController = (req, res)=>{
	console.log(req.body)
	User.findOne({username:req.body.username},(err,result)=>{
		if(result !== null && result.username === req.body.username){
			return res.send("Duplicate Username Found")
		}
		else{
			let newUser = new User({
				username:req.body.username,
				password:req.body.password
			})
			newUser.save((saveErr,savedUser)=>{
				console.log(savedUser)
				if(saveErr){
					return console.error(saveErr)
				}
				else{
					return res.send("Registration Successful")
				}
			})
		}
	})
}
 
module.exports.getAllUsersController = (req,res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.getSingleUserController = (req,res) => {
	console.log(req.params.id)
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateSingleUserController = (req,res) => {

	console.log(req.params.id)
	let updates = {
		username: req.body.username
	}
	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))
}